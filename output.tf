##################################################################################
# OUTPUT
##################################################################################

output "aws_elb_public_dns" {
  value = aws_elb.web.dns_name
}

output "aws_route53_fqdn" {
  value = aws_route53_record.www.fqdn
}