resource "aws_route53_record" "www" {
  zone_id = data.aws_route53_zone.northernlightsio.zone_id
  name    = "${var.domain_name[terraform.workspace]}.${data.aws_route53_zone.northernlightsio.name}"
  type    = "CNAME"
  ttl     = "300"
  records = [aws_elb.web.dns_name]
}
