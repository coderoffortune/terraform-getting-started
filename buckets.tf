# S3 Bucket config#
module "bucket" {
  name = local.s3_bucket_name
  
  source      = "./Modules/s3"
  common_tags = local.common_tags
}

resource "aws_s3_bucket_object" "website" {
  bucket = module.bucket.bucket
  key    = "/website/index.html"
  source = "./website/index.html"
}

resource "aws_s3_bucket_object" "graphic" {
  bucket = module.bucket.bucket
  key    = "/website/Globo_logo_Vert.png"
  source = "./website/Globo_logo_Vert.png"
}
